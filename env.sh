export LC_CTYPE=en_US.UTF-8
export CLICOLOR=true

export EDITOR="mvim"
export HISTSIZE=250000
export PROMPT_COMMAND="history -a"

export JAVA_HOME=/usr
export SCRIPTS_HOME=~/proj/scripts

export PATH=/usr/local/bin:/opt/local/bin:/opt/local/sbin:~/bin:$SCRIPTS_HOME:$PATH

